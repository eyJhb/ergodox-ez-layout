#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_extras/keymap_dvorak.h" // dvorak programming layout

#define BASE 0 // default layer
#define SYMB 1 // symbols
#define MDIA 2 // media keys

enum custom_keycodes {
#ifdef ORYX_CONFIGURATOR
  EPRM = EZ_SAFE_RANGE,
#else
  EPRM = SAFE_RANGE,
#endif
  VRSN,
  RGB_SLD
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Keymap 0: Basic layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |   ~    |   1  |   2  |   3  |   4  |   5  |  ^   |           |   $  |   6  |   7  |   8  |   9  |   0  |   +    |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |   |    |   "  |   ,  |   .  |   P  |   Y  |  [{  |           |  }]  |   F  |   G  |   C  |   R  |   L  |   -    |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | LSHIFT |   A  |   O  |   E  |   U  |   I  |------|           |------|   D  |   H  |   T  |   N  |S/mdia| RSHIFT |
 * |--------+------+------+------+------+------| SLCK |           | SLCK |------+------+------+------+------+--------|
 * |CTRL/tab|   :  |   Q  |   J  |   K  |   X  |      |           |      |   B  |   M  |   W  |   V  |   Z  |CTRL/tab|
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   | ESC  |      | left | right| super|                                       | super|  up  | down |      |TG(SYMB)|
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        | CAPS |      |       |      |   `    |
 *                                 ,------|------|------|       |------+--------+------.
 *                                 |      |      |      |       |      |        |      |
 *                                 | Space| BKSP |------|       |------|LT(SYMB)|Enter |
 *                                 |      |      | LALT |       | RALT |   tab  |      |
 *                                 `--------------------'       `----------------------'
 */
[BASE] = LAYOUT_ergodox(
  // left hand
  DV_TILD,         DV_1,        DV_2,          DV_3,    DV_4,    DV_5,    DV_CIRC,
  DV_BSLS,         DV_QUOT,     DV_COMM,       DV_DOT,  DV_P,    DV_Y,    DV_LBRC,
  KC_LSFT,         DV_A,        DV_O,          DV_E,    DV_U,    DV_I,
  LCTL_T(KC_TAB),  DV_SCLN,     DV_Q,          DV_J,    DV_K,    DV_X,    KC_SLCK,
  KC_ESC,          KC_NO,       KC_LEFT,       KC_RIGHT,KC_LGUI,
                                                                 KC_CAPSLOCK, KC_NO,
                                                                          KC_NO,
                                                         KC_SPC, KC_BSPC, KC_LALT,
  // right hand
  DV_DLR,       DV_6,    DV_7,    DV_8,    DV_9,              DV_0,           DV_SLSH,
  DV_RBRC,      DV_F,    DV_G,    DV_C,    DV_R,              DV_L,           DV_EQL,
                DV_D,    DV_H,    DV_T,    DV_N,              LT(MDIA, DV_S), RSFT_T(DV_MINS),
  KC_SLCK,      DV_B,    DV_M,    DV_W,    DV_V,              DV_Z,           RCTL_T(KC_TAB),
                KC_LGUI, KC_UP,   KC_DOWN, KC_NO,   TG(SYMB),
  KC_NO, DV_GRV,
  KC_NO,
  KC_RALT, LT(SYMB, KC_TAB), KC_ENT
),
/* Keymap 1: Symbol Layer
 *
 * ,---------------------------------------------------.           ,--------------------------------------------------.
 * |Version  |  F1  |  F2  |  F3  |  F4  |  F5  |      |           |      |  F6  |  F7  |  F8  |  F9  |  F10 |   F11  |
 * |---------+------+------+------+------+------+------|           |------+------+------+------+------+------+--------|
 * |         |   !  |   @  |   {  |   }  |   |  |      |           |      |   Up |   7  |   8  |   9  |   *  |   F12  |
 * |---------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |         |   #  |   $  |   (  |   )  |   `  |------|           |------| Down |   4  |   5  |   6  |   +  |        |
 * |---------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |         |   %  |   ^  |   [  |   ]  |   ~  |      |           |      |   &  |   1  |   2  |   3  |   \  |        |
 * `---------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   | EPRM  |      |      |      |      |                                       |   0  |   .  |   ,  |   =  |      |
 *   `-----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |Animat|      |       |Toggle|Solid |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |Bright|Bright|      |       |      |Hue-  |Hue+  |
 *                                 |ness- |ness+ |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[SYMB] = LAYOUT_ergodox(
  // left hand
  VRSN,    KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_TRNS,
  KC_TRNS, DV_EXLM, DV_AT,   DV_LCBR, DV_RCBR, DV_PIPE, KC_TRNS,
  KC_TRNS, DV_HASH, DV_DLR,  DV_LPRN, DV_RPRN, DV_GRV,
  KC_TRNS, DV_PERC, DV_CIRC, DV_LBRC, DV_RBRC, DV_TILD, KC_TRNS,
  EPRM,    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                                               RGB_MOD, KC_TRNS,
                                                        KC_TRNS,
                                      RGB_VAD, RGB_VAI, KC_TRNS,
  // right hand
  KC_TRNS, KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,
  KC_TRNS, KC_UP,   KC_7,    KC_8,    KC_9,    DV_ASTR, KC_F12,
           KC_DOWN, KC_4,    KC_5,    KC_6,    KC_PLUS, KC_TRNS,
  KC_TRNS, DV_AMPR, KC_1,    KC_2,    KC_3,    KC_BSLS, KC_TRNS,
                    KC_0,    DV_DOT,  DV_COMM, DV_EQL,  KC_TRNS,
  RGB_TOG, RGB_SLD,
  KC_TRNS,
  KC_TRNS, RGB_HUD, RGB_HUI
),

/* Keymap 2: Layer for mouse and media keys
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      | M_UP |      |      |      |           | TRNS |      |Audio-|Audio+| Mute |alt+f4|        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      | M_LE | M_DN | M_RI |      |------|           |------| Play | SN<- |->SN  |M_ACEL| TRNS |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 | Mouse| Mouse|      |       |      |Scroll|Scroll|
 *                                 | Left | Right|------|       |------| Down |  Up  |
 *                                 | Click| Click|      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */

[MDIA] = LAYOUT_ergodox(
    // left hand
    _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, KC_MS_UP, _______, _______, _______,
    _______, _______, KC_MS_LEFT, KC_MS_DOWN, KC_MS_RIGHT, _______,
    _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______,
                                         _______, _______,
                                                _______,
                                  KC_MS_BTN1, KC_MS_BTN2, _______,
    // right hand
    _______, _______, _______, _______, _______, _______,_______,
    KC_TRNS, _______, KC_VOLD, KC_VOLU, KC_MUTE, LALT(KC_F4), _______,
	         KC_MPLY, KC_MPRV, KC_MNXT, KC_ACL0, KC_TRNS, _______,
    _______, _______, _______, _______, _______, _______, _______,
                      _______, _______, _______, _______, KC_TRNS,
        _______, _______,
        _______,
        _______, KC_WH_D, KC_WH_U
),
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
    switch (keycode) {
      case EPRM:
        eeconfig_init();
        return false;
      case VRSN:
        SEND_STRING (QMK_KEYBOARD "/" QMK_KEYMAP " @ " QMK_VERSION);
        return false;
      #ifdef RGBLIGHT_ENABLE
      case RGB_SLD:
        rgblight_mode(1);
        return false;
      #endif
    }
  }
  return true;
}

// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {
#ifdef RGBLIGHT_COLOR_LAYER_0
  rgblight_setrgb(RGBLIGHT_COLOR_LAYER_0);
#endif
};

// Runs whenever there is a layer state change.
layer_state_t layer_state_set_user(layer_state_t state) {
  ergodox_board_led_off();
  ergodox_right_led_1_off();
  ergodox_right_led_2_off();
  ergodox_right_led_3_off();

  uint8_t layer = get_highest_layer(state);
  switch (layer) {
      case 0:
        #ifdef RGBLIGHT_COLOR_LAYER_0
          rgblight_setrgb(RGBLIGHT_COLOR_LAYER_0);
        #else
        #ifdef RGBLIGHT_ENABLE
          rgblight_init();
        #endif
        #endif
        break;
      case 1:
        ergodox_right_led_1_on();
        #ifdef RGBLIGHT_COLOR_LAYER_1
          rgblight_setrgb(RGBLIGHT_COLOR_LAYER_1);
        #endif
        break;
      case 2:
        ergodox_right_led_2_on();
        #ifdef RGBLIGHT_COLOR_LAYER_2
          rgblight_setrgb(RGBLIGHT_COLOR_LAYER_2);
        #endif
        break;
      case 3:
        ergodox_right_led_3_on();
        #ifdef RGBLIGHT_COLOR_LAYER_3
          rgblight_setrgb(RGBLIGHT_COLOR_LAYER_3);
        #endif
        break;
      case 4:
        ergodox_right_led_1_on();
        ergodox_right_led_3_on();
        #ifdef RGBLIGHT_COLOR_LAYER_4
          rgblight_setrgb(RGBLIGHT_COLOR_LAYER_4);
        #endif
        break;
      case 5:
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        #ifdef RGBLIGHT_COLOR_LAYER_5
          rgblight_setrgb(RGBLIGHT_COLOR_LAYER_5);
        #endif
        break;
      case 6:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        #ifdef RGBLIGHT_COLOR_LAYER_6
          rgblight_setrgb(RGBLIGHT_COLOR_LAYER_6);
        #endif
        break;
      default:
        break;
    }

  return state;
};
