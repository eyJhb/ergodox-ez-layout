# eyJhbs keyboard layout

```
git clone https://github.com/qmk/qmk_firmware.git
cd qmk_firmware
git submodule update --init

mkdir -p keyboards/ergodox_ez/keymaps/eyjhbdvorak/
ln -s $(pwd)../default/keymap.c keyboards/ergodox_ez/keymaps/eyjhbdvorak/
nix-shell ./shell.nix

make ergodox_ez:eyjhbdvorak
sudo teensy-loader-cli -mmcu=atmega32u4 -w ergodox_ez_eyjhbdvorak.hex -v
```
